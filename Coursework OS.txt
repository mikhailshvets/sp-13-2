List of employed topics for courseworks:

* For ease of use, numbers of reserved topics are in square brackets*

 1 Two-pass assembler

 2 File system defragmenter 

[3] System resources monitoring  -  K. Kulikova

 4 Process and thread manager for Windows

[5] File manager for Windows  -  N. Trofimova

 6 Format of pe-executable 

[7] Starting different programs at set time  -  V. Petrikovskaya

[8] Utility to save information from system register on installed programs in a file  -  A. Okhmat

 9 Utility for Windows system register clean-up

[10] Utility to track changes in the file system  -  A. Triukhan

 11 Partition table and FAT viewer

 12 MaxSplitter utility 
 
[13] Remote control utility  -  B. Ovsyannikov

[14] Simple mail program based on SMTP protocol - K. Alabtakh
 
[15] FTP client  -  V. Bakasov
 
[16] FTP server  -  M. Fedorenchik
 
 17 Windows system services
 
 18 COM-server for network system testing
 
 19
 
 20 The OLE automation server

[21] The operating system scheduler emulator  -  A. Storcheus

 22  Development of your own file system
 
 23  Implementation of a security code in the pe-file